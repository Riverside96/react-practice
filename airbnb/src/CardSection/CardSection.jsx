import React from 'react'
import Card from '../Card/Card.jsx'
import './Card-Section.css'
import dat from '../data.js'

const cards = dat.map(card =>
   <Card
      key={card.id}
      {...card}
   />
)

const CardSection = () => {
  return (
    <div className='CardSection--wrapper'>
      {cards}
    </div>
  )
}

export default CardSection
