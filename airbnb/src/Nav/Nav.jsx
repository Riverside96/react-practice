import React from 'react'
import './Nav.css'
import logo from '../assets/pngwing.com.png'

const App = () => {
  return (
    <div className='nav-wrapper'>
      <img src={logo} height='100px' className='nav-logo' />
    </div>
  )
}

export default App
