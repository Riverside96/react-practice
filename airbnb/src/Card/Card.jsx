import React from 'react'
import './Card.css'
import star from '../assets/star.png'
const Card = ({id, title, description, price, coverImg, stats, location, openSpots}) => {

  let badgeTxt
  if(openSpots===0){ badgeTxt = "Sold Out"}
  else if(location === "Online"){ badgeTxt = "Online"}

  return (
    <div className='card-wrapper'>
      {badgeTxt && <div className='card-wrapper-soldout'>{badgeTxt}</div>}
      <img src={coverImg} width='230px' height="360px" alt="" className='card-image' />
      <div className='card-body'>

         <div className='card--stats'>
            <img src={star} width='17px' height='17px' alt="" className='card--star' />
            <span>{stats.rating}</span>
            <span>·</span>
            <span>({stats.reviewCount}){location}</span>
         </div>
         <p>{title}</p>
         <p> <span className='bold'>From ${price} </span> / person</p>
      </div>
    </div>
  )
}

export default Card
