import React from 'react'
import gridImg from '../assets/photo-grid.png'
import './Hero.css'

const Hero = () => {
  return (
    <div className='hero-wrapper'>
      <img src={gridImg} height='400px' alt="" />
      <div className='hero-text-section'>
         <h1>Online Experiences</h1>
         <h3>Join unique interactive activities led by one-of-a-kind-hosts, all without leaving home</h3>
      </div>
    </div>
  )
}

export default Hero
