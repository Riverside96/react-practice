import React from 'react'
import ReactDOM from 'react-dom/client'
import './main.css'
import Nav from './Nav/Nav.jsx'
import Hero from './Hero/Hero.jsx'
import CardSection from './CardSection/CardSection.jsx'

ReactDOM.createRoot(document.getElementById('root')).render(
  <React.StrictMode>
    <Nav />
    <Hero />
    <CardSection />
  </React.StrictMode>,
)
