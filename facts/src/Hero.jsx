import './Hero.css'

const Hero = () => {
  return (
    <div className='hero-container'>
       <h1 className='hero-title'>Fun facts about React</h1>
       <ul className='list'>
         <li>Was first released in 2013</li>
         <li>Was originally created by Jordan Walke</li>
         <li>Has well over 100k stars</li>
         <li>Is maintained by a large company (Facebook)</li>
         <li>Powers thousands of enterprise apps</li>
       </ul>
         
    </div>
  )
}

export default Hero
