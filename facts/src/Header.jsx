import reactLogo from './assets/react.svg'
import './Header.css'

export default function Header() {
  return (
    <>
      <div className='nav-container'>  
         <nav className='nav'>
            <div className='lhs'>
               <img src={reactLogo} alt="logo"/>
               <h3>ReactFacts</h3>
            </div>
            <div className='rhs'>
               <h3>React Course - Project 1</h3>
            </div>
         </nav>
      </div>
    </>
  )
}


