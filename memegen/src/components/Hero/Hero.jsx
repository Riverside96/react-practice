import React from 'react'
import './Hero.css'
import rupees from '../../assets/rupees.jpg'
import dat from '../Data/memeDat.js'

const Hero = () => {
  const [allMemeImages, setAllMemeImages] = React.useState(dat)

  const n = Math.floor(Math.random() * allMemeImages.data.memes.length)
  const [meme, setMeme] = React.useState({
         topText:"",
         bottomText:"",
         randomImg:allMemeImages.data.memes[n].url
   })

  function generateMeme(){
      const memeArr = allMemeImages.data.memes
      const n = Math.floor(Math.random() * memeArr.length)
      const url = memeArr[n].url
      setMeme(prevMeme => ({
         ...prevMeme,
         randomImg:url
      }))
      return 
   }

   function handleChange(event){
      const {name, value} = event.target
      setMeme(prev => ({
         ...prev,
         [name] : value
      }))
   }
   
  return (
    <div className='hero'>
      
      <div className='hero--textboxes'>
         <input 
               className='hero--textboxes--lhs' 
               type="text"
               placeholder='Top Text'
               name="topText"
               value={meme.topText}
               onChange={handleChange}

         />  
         <input 
               className='hero--textboxes--rhs' 
               type="text" 
               placeholder='Bottom Text'
               name="bottomText"
               value={meme.bottomText}
               onChange={handleChange}
         />  
      </div>
      
      <button
         onClick={(generateMeme)}
         type="submit">Get a new meme image
      </button>
      
      <div className='hero--meme'>        
         <img className='hero--meme--img' src={meme.randomImg} alt="meme image"/>
         <h2 className='hero--meme--text hero--meme--text--top'>{meme.topText}</h2>
         <h2 className='hero--meme--text hero--meme--text--bottom'>{meme.bottomText}</h2>
     </div>

   </div>
  )
}

export default Hero
