import React from 'react'
import './Header.css'
import troll from '../../assets/troll.svg'

const Header = () => {
  return (
    <div className='header'>
      <div className='header--lhs'>
         <img src={troll} alt="troll-logo"/>
         <h1>Meme Generator</h1>
      </div>
      <div className='header--rhs'>
         <h3>React Course - Project 3</h3>
      </div>
    </div>
  )
}

export default Header
