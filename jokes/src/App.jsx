import React from 'react'
import Joke from './Joke'

const App = () => {
  return (
    <div>
      <Joke
         num="1"
         setup="I got my daughter a fridge for her birthday"
         punchline="I can't wait to see her face light up when she opens it."
         upvotes={3}
         downvotes={1}
         comments= {[
            "nice joke",
            "Sick joke bruh"
         ]}
         isPun={false}
      />
      <Joke
         num="2"
         punchline="Thats what she said!"
         upvotes={5}
         downvotes={1}
         comments= {[
            "Office jokes are the best mang"
         ]}
         isPun={false}
      />
      <Joke
         num="3"
         setup="Time flies like an arrow"
         punchline="Fruit flies like a banana"
         upvotes={7}
         downvotes={3}
         comments= {[
         ]}
         isPun={true}
      />
    </div>
  )
}

export default App



