import React from 'react'

const Joke = (props) => {
  let jokeType
  jokeType = props.isPun == true ? "pun" : "not pun";
  return (
    <div>
      <h3>Joke Number {props.num}</h3>
      {props.setup && <p>Setup: {props.setup}</p>}
      <p>Punchline: {props.punchline}</p>
      {props.comments != null && <p>Comments: {props.comments}</p>}
      <p>Upvotes: {props.upvotes}</p>
      <p>Downvotes: {props.downvotes}</p>
      <p>Joke Type: {jokeType}</p>
    </div>
  )
}

export default Joke



